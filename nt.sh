#!/bin/bash
read -p 您想测试哪些ISP的路由？$'\n'1.移动$'\n'2.联通$'\n'3.电信$'\n'4.三网$'\n'请选择选项: x
read -p 请您选择要测试的IP类型$'\n'1.IPv4$'\n'2.IPv6$'\n'3.IPv4+IPv6$'\n'请选择选项: y

if [ -z $x ]&&[ -z $y ]
then
   nexttrace cqv4.cm.123615.xyz
elif [ $x == 1 ]&&[ $y == 1 ]
then
   nexttrace cqv4.cm.123615.xyz
elif [ $x == 1 ]&&[ $y == 2 ]
then
   nexttrace cqv6.cm.123615.xyz
elif [ $x == 1 ]&&[ $y == 3 ]
then
   nexttrace cqv4.cm.123615.xyz && nexttrace cqv6.cm.123615.xyz
elif [ $x == 2 ]&&[ $y == 1 ]
then
   nexttrace cqv4.cu.123615.xyz
elif [ $x == 2 ]&&[ $y == 2 ]
then
   nexttrace cqv6.cu.123615.xyz
elif [ $x == 2 ]&&[ $y == 3 ]
then
   nexttrace cqv4.cu.123615.xyz && nexttrace cqv6.cu.123615.xyz
elif [ $x == 3 ]&&[ $y == 1 ]
then
   nexttrace cqv4.ct.123615.xyz
elif [ $x == 3 ]&&[ $y == 2 ]
then
   nexttrace cqv6.ct.123615.xyz
elif [ $x == 3 ]&&[ $y == 3 ]
then
   nexttrace cqv4.ct.123615.xyz && nexttrace cqv6.ct.123615.xyz
elif [ $x == 4 ]&&[ $y == 1 ]
then
   nexttrace cqv4.cm.123615.xyz && nexttrace cqv4.cu.123615.xyz && nexttrace cqv4.ct.123615.xyz
elif [ $x == 4 ]&&[ $y == 2 ]
then
   nexttrace cqv6.cm.123615.xyz && nexttrace cqv6.cu.123615.xyz && nexttrace cqv6.ct.123615.xyz
elif [ $x == 4 ]&&[ $y == 3 ]
then
   nexttrace cqv4.cm.123615.xyz && nexttrace cqv4.cu.123615.xyz && nexttrace cqv4.ct.123615.xyz && nexttrace cqv6.cm.123615.xyz && nexttrace cqv6.cu.123615.xyz && nexttrace cqv6.ct.123615.xyz
else
   break
fi
